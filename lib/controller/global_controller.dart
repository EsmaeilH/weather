import 'package:get/get.dart';
import 'package:geolocator/geolocator.dart';

import '../api/fetch_weather.dart';
import '../model/weather_data.dart';
import '../resources/string_manager.dart';

class GlobalController extends GetxController {
  // create variables
  final RxBool _isLoading = true.obs;
  final RxBool _isLocationPermitted = true.obs;
  final RxDouble _latitude = 0.0.obs;
  final RxDouble _longitude = 0.0.obs;
  final RxInt _currentIndex = 0.obs;

  // create getter for variable
  RxBool get checkLoading => _isLoading;

  RxBool get checkLocationPermission => _isLocationPermitted;

  RxDouble get latitude => _latitude;

  RxDouble get longitude => _longitude;

  RxInt get currentIndex => _currentIndex;

  final weatherData = WeatherData().obs;

  WeatherData getData() {
    return weatherData.value;
  }

  @override
  void onInit() {
    getPermission();
    super.onInit();
  }

  getPermission() async {
    LocationPermission locationPermission;

    // Permission check
    locationPermission = await Geolocator.checkPermission();

    if (locationPermission == LocationPermission.deniedForever) {
      _isLocationPermitted.value = false;
      return;
    } else if (locationPermission == LocationPermission.denied) {
      // request permission
      locationPermission = await Geolocator.requestPermission();
      if (locationPermission == LocationPermission.denied) {
        _isLocationPermitted.value = false;
        return;
      }
    }

    _isLocationPermitted.value = true;
    if (_isLoading.isTrue) {
      getLocation();
    }
  }

  getLocation() async {
    // getting the current position
    try {
      return await Geolocator.getCurrentPosition(
              desiredAccuracy: LocationAccuracy.high)
          .then((value) {
        // update our latitude and longitude
        _latitude.value = value.latitude;
        _longitude.value = value.longitude;
        // calling our weather api
        return FetchWeatherApi()
            .processData(value.latitude, value.longitude)
            .then((value) {
          weatherData.value = value;
          _isLoading.value = false;
        });
      });
    } catch (e) {
      e.printError();
      Get.snackbar(
          Strings.snackBarNetworkErrorTitle, Strings.snackBarNetworkErrorBody,
          snackPosition: SnackPosition.BOTTOM);
    }
  }
}
