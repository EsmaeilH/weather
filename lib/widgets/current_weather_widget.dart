import 'package:flutter/material.dart';
import 'package:weather/model/weather_data_current.dart';
import 'package:weather/model/weather_data_hourly.dart';

import '../resources/custom_colors.dart';
import '../resources/value_manager.dart';

class CurrentWeatherWidget extends StatelessWidget {
  final WeatherDataHourly weatherDataHourly;

  const CurrentWeatherWidget({super.key, required this.weatherDataHourly});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // temperature area
        temperatureAreaWidget(),

        const SizedBox(
          height: AppSize.s20,
        ),
        // more details - windSpeed, humidity, clouds
        currentWeatherMoreDetailsWidget()
      ],
    );
  }

  Widget temperatureAreaWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Image.asset(
          "assets/weather/${weatherDataHourly.hourly[0].weather![0].icon}.png",
          height: AppSize.s80,
          width: AppSize.s80,
        ),
        Container(
          height: AppSize.s50,
          width: AppSize.s1,
          color: CustomColors.dividerLine,
        ),
        RichText(
            text: TextSpan(children: [
          TextSpan(
              text: "${weatherDataHourly.hourly[0].main!.temp!.toInt()}°",
              style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: AppSize.s68,
                  color: CustomColors.textColorBlack)),
          TextSpan(
              text: "${weatherDataHourly.hourly[0].weather![0].description}°",
              style: const TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: AppSize.s14,
                  color: CustomColors.tempDescriptionColor))
        ]))
      ],
    );
  }

  Widget currentWeatherMoreDetailsWidget() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: AppSize.s60,
              width: AppSize.s60,
              padding: const EdgeInsets.all(AppSize.s16),
              decoration: BoxDecoration(
                  color: CustomColors.cardColor,
                  borderRadius: BorderRadius.circular(AppSize.s16)),
              child: Image.asset("assets/icons/windspeed.png"),
            ),
            Container(
              height: AppSize.s60,
              width: AppSize.s60,
              padding: const EdgeInsets.all(AppSize.s16),
              decoration: BoxDecoration(
                  color: CustomColors.cardColor,
                  borderRadius: BorderRadius.circular(AppSize.s16)),
              child: Image.asset("assets/icons/clouds.png"),
            ),
            Container(
              height: AppSize.s60,
              width: AppSize.s60,
              padding: const EdgeInsets.all(AppSize.s16),
              decoration: BoxDecoration(
                  color: CustomColors.cardColor,
                  borderRadius: BorderRadius.circular(AppSize.s16)),
              child: Image.asset("assets/icons/humidity.png"),
            )
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
              height: AppSize.s20,
              width: AppSize.s60,
              child: Text(
                "${weatherDataHourly.hourly[0].windSpeed!.speed}km/h",
                style: const TextStyle(fontSize: AppSize.s12),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: AppSize.s20,
              width: AppSize.s60,
              child: Text(
                "${weatherDataHourly.hourly[0].clouds!.clouds}%",
                style: const TextStyle(fontSize: AppSize.s12),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: AppSize.s20,
              width: AppSize.s60,
              child: Text(
                "${weatherDataHourly.hourly[0].main!.humidity}%",
                style: const TextStyle(fontSize: AppSize.s12),
                textAlign: TextAlign.center,
              ),
            )
          ],
        )
      ],
    );
  }
}
