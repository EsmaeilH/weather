import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:weather/controller/global_controller.dart';
import 'package:weather/resources/value_manager.dart';

import '../resources/string_manager.dart';

class HeaderWidget extends StatefulWidget {
  const HeaderWidget({super.key});

  @override
  State<HeaderWidget> createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  String city = '';
  String date = DateFormat("yMMMMd").format(DateTime.now());
  final GlobalController globalController =
      Get.put(GlobalController(), permanent: true);

  @override
  void initState() {
    getAddress(
        globalController.latitude.value, globalController.longitude.value);
    super.initState();
  }

  void getAddress(lat, long) async {
    try {
      List<Placemark> placeMark = await placemarkFromCoordinates(lat, long);
      Placemark place = placeMark[0];
      setState(() {
        city = place.locality!;
      });
    } catch (e) {
      /// adding snackBar here
      e.printError();
      // Get.snackbar(
      //     Strings.snackBarNetworkErrorTitle, Strings.snackBarNetworkErrorBody,
      //     snackPosition: SnackPosition.BOTTOM);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin:
              const EdgeInsets.only(left: AppMargin.m20, right: AppMargin.m20),
          alignment: Alignment.topLeft,
          child: Text(
            city,
            style: const TextStyle(fontSize: AppSize.s36, height: AppSize.s2),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
              left: AppMargin.m20, right: AppMargin.m20, bottom: AppMargin.m20),
          alignment: Alignment.topLeft,
          child: Text(
            date,
            style: TextStyle(
                fontSize: AppSize.s14,
                color: Colors.grey[700],
                height: AppSize.s1),
          ),
        )
      ],
    );
  }
}
