import 'package:flutter/material.dart';
import 'package:weather/model/weather_data_current.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'package:weather/model/weather_data_hourly.dart';

import '../resources/custom_colors.dart';
import '../resources/value_manager.dart';
import 'package:weather/resources/string_manager.dart';

class ComfortLevel extends StatelessWidget {
  final WeatherDataHourly weatherDataHourly;

  const ComfortLevel({super.key, required this.weatherDataHourly});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.fromLTRB(
              AppMargin.m20, AppMargin.m1, AppMargin.m20, AppMargin.m20),
          child: const Text(
            Strings.comfortLevel,
            style: TextStyle(fontSize: AppSize.s18),
          ),
        ),
        SizedBox(
          height: AppSize.s180,
          child: Column(
            children: [
              Center(
                child: SleekCircularSlider(
                  min: AppConstant.c0.toDouble(),
                  max: AppConstant.c100.toDouble(),
                  initialValue: weatherDataHourly.hourly[0].main!.humidity!.toDouble(),
                  appearance: CircularSliderAppearance(
                      customWidths: CustomSliderWidths(
                          handlerSize: AppConstant.c0.toDouble(),
                          trackWidth: AppSize.s12,
                          progressBarWidth: AppSize.s12),
                      infoProperties: InfoProperties(
                          bottomLabelText: Strings.humidity,
                          bottomLabelStyle: const TextStyle(
                              letterSpacing: 0.1,
                              fontSize: AppSize.s14,
                              height: AppSize.s1)),
                      animationEnabled: true,
                      size: AppSize.s140,
                      customColors: CustomSliderColors(
                          hideShadow: true,
                          trackColor: CustomColors.firstGradientColor
                              .withAlpha(AppConstant.c100),
                          progressBarColors: [
                            CustomColors.firstGradientColor,
                            CustomColors.secondGradientColor
                          ])),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RichText(
                      text: TextSpan(children: [
                    const TextSpan(
                        text: Strings.feelsLike,
                        style: TextStyle(
                            fontSize: AppSize.s14,
                            height: 0.8,
                            color: CustomColors.textColorBlack,
                            fontWeight: FontWeight.w400)),
                    TextSpan(
                        text: "${weatherDataHourly.hourly[0].main!.feelsLike}",
                        style: const TextStyle(
                            fontSize: AppSize.s14,
                            height: 0.8,
                            color: CustomColors.textColorBlack,
                            fontWeight: FontWeight.w400))
                  ])),
                  Container(
                    height: AppSize.s25,
                    width: AppSize.s1,
                    margin: const EdgeInsets.only(
                        left: AppMargin.m40, right: AppMargin.m40),
                    color: CustomColors.dividerLine,
                  ),
                  RichText(
                      text: TextSpan(children: [
                    const TextSpan(
                        text: Strings.pressure,
                        style: TextStyle(
                            fontSize: AppSize.s14,
                            height: 0.8,
                            color: CustomColors.textColorBlack,
                            fontWeight: FontWeight.w400)),
                    TextSpan(
                        text: "${weatherDataHourly.hourly[0].main!.pressure}",
                        style: const TextStyle(
                            fontSize: AppSize.s14,
                            height: 0.8,
                            color: CustomColors.textColorBlack,
                            fontWeight: FontWeight.w400))
                  ])),
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}
