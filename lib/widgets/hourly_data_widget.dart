import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:weather/controller/global_controller.dart';
import 'package:weather/resources/custom_colors.dart';
import 'package:weather/resources/value_manager.dart';

import '../model/weather_data_hourly.dart';
import '../resources/string_manager.dart';

class HourlyDataWidget extends StatelessWidget {
  final WeatherDataHourly weatherDataHourly;

  HourlyDataWidget({super.key, required this.weatherDataHourly});

  RxInt cardIndex = GlobalController().currentIndex;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(
              vertical: AppMargin.m8, horizontal: AppMargin.m20),
          alignment: Alignment.topCenter,
          child: const Text(
            Strings.today,
            style: TextStyle(fontSize: AppSize.s18),
          ),
        ),
        hourlyList(),
      ],
    );
  }

  Widget hourlyList() {
    return Container(
      height: AppSize.s160,
      padding:
          const EdgeInsets.only(top: AppPadding.p10, bottom: AppPadding.p10),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: weatherDataHourly.hourly.length > 12
            ? 12
            : weatherDataHourly.hourly.length,
        itemBuilder: (context, index) {
          return Obx(() => GestureDetector(
                onTap: () {
                  cardIndex.value = index;
                },
                child: Container(
                  width: AppSize.s90,
                  margin: const EdgeInsets.only(
                      left: AppMargin.m20, right: AppMargin.m5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(AppSize.s12),
                      boxShadow: [
                        BoxShadow(
                            offset: const Offset(0.5, 0),
                            blurRadius: 30,
                            spreadRadius: 1,
                            color: CustomColors.dividerLine.withAlpha(150)),
                      ],
                      gradient: cardIndex.value == index
                          ? const LinearGradient(colors: [
                              CustomColors.firstGradientColor,
                              CustomColors.secondGradientColor
                            ])
                          : null),
                  child: HourlyDetails(
                    temp: weatherDataHourly.hourly[index].main!.temp!,
                    timeStamp: weatherDataHourly.hourly[index].dt!,
                    index: index,
                    cardIndex: cardIndex.toInt(),
                    weatherIcon:
                        weatherDataHourly.hourly[index].weather![0].icon!,
                  ),
                ),
              ));
        },
      ),
    );
  }
}

// hourly details class
class HourlyDetails extends StatelessWidget {
  int temp;
  int timeStamp;
  int index;
  int cardIndex;
  String weatherIcon;

  HourlyDetails(
      {super.key,
      required this.temp,
      required this.timeStamp,
      required this.index,
      required this.cardIndex,
      required this.weatherIcon});

  String getTime(final timeStamp) {
    DateTime time = DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
    String x = DateFormat('jm').format(time);
    return x;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          margin: const EdgeInsets.only(top: AppMargin.m10),
          child: Text(
            getTime(timeStamp),
            style: TextStyle(
                color: cardIndex == index
                    ? CustomColors.selectedCardColor
                    : CustomColors.textColorBlack),
          ),
        ),
        Container(
          margin: const EdgeInsets.all(AppMargin.m5),
          height: AppSize.s40,
          width: AppSize.s40,
          child: Image.asset("assets/weather/$weatherIcon.png"),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: AppMargin.m10),
          child: Text(
            "$temp°",
            style: TextStyle(
                color: cardIndex == index
                    ? CustomColors.selectedCardColor
                    : CustomColors.textColorBlack),
          ),
        )
      ],
    );
  }
}
