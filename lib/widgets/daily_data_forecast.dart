import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather/model/weather_data_daily.dart';
import 'package:weather/model/weather_data_hourly.dart';

import '../resources/custom_colors.dart';
import '../resources/string_manager.dart';
import '../resources/value_manager.dart';

class DailyDataForecast extends StatelessWidget {
  final WeatherDataHourly weatherDataHourly;

  const DailyDataForecast({super.key, required this.weatherDataHourly});

  // string manipulation
  String getDay(final day) {
    DateTime time = DateTime.fromMillisecondsSinceEpoch(day * 1000);
    final x = DateFormat('EEE').format(time);
    return x;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: AppSize.s400,
      margin: const EdgeInsets.all(AppMargin.m20),
      padding: const EdgeInsets.all(AppPadding.p14),
      decoration: BoxDecoration(
          color: CustomColors.dividerLine.withAlpha(150),
          borderRadius: BorderRadius.circular(AppSize.s20)),
      child: Column(
        children: [
          Container(
            alignment: Alignment.topLeft,
            margin: const EdgeInsets.only(bottom: AppMargin.m10),
            child: const Text(
              Strings.nextDays,
              style: TextStyle(
                  color: CustomColors.textColorBlack, fontSize: AppSize.s18),
            ),
          ),
          dailyList()
        ],
      ),
    );
  }

  Widget dailyList() {
    return SizedBox(
      height: 300,
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: weatherDataHourly.hourly.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              Container(
                  height: AppSize.s60,
                  padding: const EdgeInsets.only(
                      left: AppPadding.p10, right: AppPadding.p10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        width: AppSize.s80,
                        child: Text(
                          getDay(weatherDataHourly.hourly[index].dt),
                          style: const TextStyle(
                              color: CustomColors.textColorBlack,
                              fontSize: AppSize.s14),
                        ),
                      ),
                      SizedBox(
                        height: AppSize.s30,
                        width: AppSize.s30,
                        child: Image.asset(
                            "assets/weather/${weatherDataHourly.hourly[index].weather![0].icon}.png"),
                      ),
                      Text(
                          "${weatherDataHourly.hourly[index].main!.tempMax}°/${weatherDataHourly.hourly[index].main!.tempMin}°")
                    ],
                  )),
              Container(
                height: AppSize.s1,
                color: CustomColors.dividerLine,
              )
            ],
          );
        },
      ),
    );
  }
}
