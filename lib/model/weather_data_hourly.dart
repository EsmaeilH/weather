class WeatherDataHourly {
  List<Hourly> hourly;

  WeatherDataHourly({required this.hourly});

  factory WeatherDataHourly.fromJson(Map<String, dynamic> json) =>
      WeatherDataHourly(
          hourly:
              List<Hourly>.from(json['list'].map((e) => Hourly.fromJson(e))));
}

//
// class Hourly {
//   Hourly({this.dt, this.temp, this.weather});
//
//   factory Hourly.fromJson(dynamic json) => Hourly(
//       dt: json['dt'],
//       temp: (json['temp'] as num?)?.round(),
//       weather: (json['weather'] as List<dynamic>?)
//           ?.map((e) => Weather.fromJson(e as Map<String, dynamic>))
//           .toList());
//
//   int? dt;
//   int? temp;
//   List<Weather>? weather;
//
//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['dt'] = dt;
//     map['temp'] = temp;
//     if (weather != null) {
//       map['weather'] = weather?.map((v) => v.toJson()).toList();
//     }
//     return map;
//   }
// }
//


class Hourly {
  Hourly({
    this.dt,
    this.main,
    this.weather,
    this.windSpeed,
    this.clouds,
    // this.dtTxt,
  });

  factory Hourly.fromJson(dynamic json) => Hourly(
    dt : json['dt'],
    main : json['main'] != null ? Main.fromJson(json['main']) : null,
    weather: (json['weather'] as List<dynamic>?)
      ?.map((e) => Weather.fromJson(e as Map<String, dynamic>))
      .toList(),
    windSpeed : json['wind'] != null ? Wind.fromJson(json['wind']) : null,
    clouds: json['clouds'] != null ? Clouds.fromJson(json['clouds']) : null,
    // dtTxt : json['dt_txt']
  );
  int? dt;
  Main? main;
  List<Weather>? weather;
  Wind? windSpeed;
  Clouds? clouds;
  // String? dtTxt;

}

class Main {
  Main({
    this.temp,
    this.feelsLike,
    this.tempMin,
    this.tempMax,
    this.pressure,
    this.humidity,});

  factory Main.fromJson(dynamic json) => Main(
      temp: (json['temp'] as num).round(),
      feelsLike : (json['feels_like'] as num).toDouble(),
      tempMin: (json['temp_min'] as num).round(),
      tempMax: (json['temp_max'] as num).round(),
      pressure : json['pressure'],
      humidity : json['humidity']
  );
  int? temp;
  double? feelsLike;
  int? tempMin;
  int? tempMax;
  int? pressure;
  int? humidity;

}

class Weather {
  Weather({
    this.id,
    this.main,
    this.description,
    this.icon,
  });

  factory Weather.fromJson(dynamic json) => Weather(
      id: json['id'],
      main: json['main'],
      description: json['description'],
      icon: json['icon']);

  int? id;
  String? main;
  String? description;
  String? icon;

}

class Wind {
  Wind({
    this.speed,});

  factory Wind.fromJson(dynamic json) => Wind(
      speed : (json['speed'] as num).toDouble()
  );

  double? speed;
}

class Clouds {
  Clouds({
    this.clouds
  });

  factory Clouds.fromJson(dynamic json) => Clouds(
      clouds: json['all']
  );

  int? clouds;
}