import 'package:weather/model/weather_data_current.dart';
import 'package:weather/model/weather_data_daily.dart';
import 'package:weather/model/weather_data_hourly.dart';

class WeatherData {
  final WeatherDataHourly? hourly;

  WeatherData([this.hourly]);

  // some function to fetch this values
  // WeatherDataCurrent get currentWeather => current!;
  WeatherDataHourly get hourlyWeather => hourly!;
  // WeatherDataDaily get dailyWeather => daily!;
}