class Strings {
  Strings._();

  static const String appTitle = "Weather";
  static const String locationPermissionNotGranted = "Location permission is not granted";
  static const String snackBarNetworkErrorTitle = "Network error";
  static const String snackBarNetworkErrorBody = "Please check internet connection";
  static const String comfortLevel = "Comfort Level";
  static const String humidity = "Humidity";
  static const String feelsLike = "Feels Like ";
  static const String pressure = "Pressure ";
  static const String nextDays = "Next Days";
  static const String today = "Today";

}