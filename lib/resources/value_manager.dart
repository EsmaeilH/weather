
class AppMargin {
  static const double m1 = 1.0;
  static const double m5 = 5.0;
  static const double m8 = 8.0;
  static const double m10 = 10.0;
  static const double m12 = 12.0;
  static const double m14 = 14.0;
  static const double m16 = 16.0;
  static const double m18 = 18.0;
  static const double m20 = 20.0;
  static const double m40 = 40.0;
}

class AppPadding {
  static const double p8 = 8.0;
  static const double p10 = 10.0;
  static const double p12 = 12.0;
  static const double p14 = 14.0;
  static const double p16 = 16.0;
  static const double p18 = 18.0;
  static const double p20 = 20.0;
}

class AppSize {
  static const double s1 = 1.5;
  static const double s2 = 2.0;
  static const double s4 = 4.0;
  static const double s8 = 8.0;
  static const double s12 = 12.0;
  static const double s14 = 14.0;
  static const double s16 = 16.0;
  static const double s18 = 18.0;
  static const double s20 = 20.0;
  static const double s25 = 25.0;
  static const double s30 = 30.0;
  static const double s36 = 36.0;
  static const double s40 = 40.0;
  static const double s50 = 50.0;
  static const double s60 = 60.0;
  static const double s68 = 68.0;
  static const double s80 = 80.0;
  static const double s90 = 90.0;
  static const double s140 = 140.0;
  static const double s160 = 160.0;
  static const double s180 = 180.0;
  static const double s400 = 400.0;
}

class AppConstant {
  static const int c0 = 0;
  static const int c100 = 100;
}