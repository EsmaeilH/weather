import '../api/api_key.dart';

String apiURL(var lat, var lon) {
  String url;
  url = "https://api.openweathermap.org/data/2.5/forecast?lat=$lat&lon=$lon&appid=$apiKey&units=metric";
  return url;
}