import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather/controller/global_controller.dart';
import 'package:weather/widgets/header_widget.dart';

import '../resources/custom_colors.dart';
import '../resources/string_manager.dart';
import '../resources/value_manager.dart';
import '../widgets/comfort_level.dart';
import '../widgets/current_weather_widget.dart';
import '../widgets/daily_data_forecast.dart';
import '../widgets/hourly_data_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // call controller
  final GlobalController globalController =
      Get.put(GlobalController(), permanent: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(() {
          if (globalController.checkLocationPermission.isTrue) {
            return globalController.checkLoading.isTrue
                ? Center(
                    child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/icons/clouds.png",
                        height: AppSize.s180,
                        width: AppSize.s180,
                      ),
                      const CircularProgressIndicator()
                    ],
                  ))
                : ListView(
                    scrollDirection: Axis.vertical,
                    children: [
                      const SizedBox(
                        height: AppSize.s20,
                      ),
                      const HeaderWidget(),
                      // for our current temp {'current'}
                      CurrentWeatherWidget(
                        weatherDataHourly:
                            globalController.getData().hourlyWeather,
                      ),
                      const SizedBox(
                        height: AppSize.s20,
                      ),
                      // for our hourly temp data {'hourly'}
                      HourlyDataWidget(
                        weatherDataHourly:
                            globalController.getData().hourlyWeather,
                      ),
                      DailyDataForecast(
                        weatherDataHourly:
                            globalController.getData().hourlyWeather,
                      ),
                      Container(
                        height: AppSize.s1,
                        color: CustomColors.dividerLine,
                      ),
                      const SizedBox(
                        height: AppSize.s12,
                      ),
                      ComfortLevel(
                        weatherDataHourly:
                            globalController.getData().hourlyWeather,
                      )
                    ],
                  );
          } else {
            return const Center(
              child: Text(Strings.locationPermissionNotGranted),
            );
          }
        }),
      ),
    );
  }
}
